package com.example.cse438.studio2.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cse438.studio2.R
import com.example.cse438.studio2.model.Product
import com.example.cse438.studio2.viewmodel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_result_list.*
import kotlinx.android.synthetic.main.result_list_item.view.*

@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {
    private var parentContext: Context = context
    private var queryString: String = query
    private var adapter = ResultProductAdapter()
    private lateinit var viewModel: ProductViewModel

    private var ProductList: ArrayList<Product> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayText = "Search for: $queryString"
        query_text.text = displayText

        //TODO: Finish implementing below based on HomeFragment
        result_items_list.layoutManager = LinearLayoutManager(parentContext)
        result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        val observer = Observer<ArrayList<Product>> {
            result_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return ProductList[p0].getId() == ProductList[p1].getId()
                }

                override fun getOldListSize(): Int {
                    return ProductList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return ProductList[p0] == ProductList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            ProductList = it ?: ArrayList()
        }

        viewModel.getProductsByQueryText(queryString).observe(this, observer)
        result_items_list.adapter = adapter
    }

    //TODO: Implement the custom adapter below
    inner class ResultProductAdapter: RecyclerView.Adapter<ResultProductAdapter.ResultProductViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultProductViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.result_list_item, p0, false)
            return ResultProductViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ResultProductViewHolder, p1: Int) {
            val product = ProductList[p1]
            val productImages = product.getImages()
            if (productImages.size == 0) {
                // Do nothing for now
            }
            else {
                Picasso.with(this@ResultListFragment.context).load(productImages[0]).into(p0.productImg)
            }
            p0.productTitle.text = product.getProductName()
            p0.productPrice.text = product.getPrice().toString()
        }

        override fun getItemCount(): Int {
            return ProductList.size
        }

        inner class ResultProductViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var productImg: ImageView = itemView.product_img
            var productTitle: TextView = itemView.product_title
            var productPrice: TextView = itemView.product_price
        }
    }
}